#include <stdio.h>



int main()
{

int x = 4;
printf("x lives at %p\n", &x);

int* address_of_x = &x;
++(*address_of_x);

printf("x has now value: %i\n",x); 

return 0;
}
