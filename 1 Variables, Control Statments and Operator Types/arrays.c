#include <stdio.h>
#include <string.h>
	 
	void main(){
	 
	printf("\n");
	     
	    char wholeName[12] = "Artur Wieczorek";

	    int primeNums[3] = {2, 3, 5,};

	    int morePrimes[] = {13, 17, 19, 23};
	     
	    printf("The first prime in the list is %d\n\n",
	        primeNums[0]);

	    char city[6] = {'P', 'o', 'z', 'n', 'a', 'n'};
	     
	    char anotherCity[5] = {'O', 'p', 'o', 'l', 'e', '\0'};
	     
	    printf("A City %s\n\n", anotherCity);
	     
	    char thirdCity[] = "Warsaw";
	     
	    char yourCity[30];
	     
	    printf("What city do you live in? ");
	     

	    fgets(yourCity, 30, stdin);
	     
	    printf("Hello %s\n\n", yourCity);

	    for(int i = 0; i < 30; i++){
	     
	        if(yourCity[i] == '\n'){
	         
	            yourCity[i] = '\0';
	            break;
	         
	        }
	     
	    }
	     
	    printf("Hello %s\n\n", yourCity);
	     
	    printf("Is your city Paris? %d\n\n",
	        strcmp(yourCity, thirdCity));
	         
	    // strcat() adds the second string to the end of the first
	     
	    char yourState[] = ", Pennsylvania";
	     
	    strcat(yourCity, yourState);
	     
	    printf("You live in %s\n\n", yourCity);
	     
	    // strlen() returns the length of the string minus \0
	     
	    printf("Letters in Paris : %d\n\n", strlen(thirdCity));
	     
	    // strcpy() can overwrite memory when copying strings.
	    // strlcpy() won't overwrite memory and it always adds a \0
	     
	    strlcpy(yourCity,
	        "El Pueblo del la Reina de Los Angeles",
	        sizeof(yourCity));
	     
	    printf("The new name is %s\n\n", yourCity);
	     
	}