	#include <stdio.h>
	 
	void main(){
	 
	    printf("\n");
	     
	    int num1 = 1, num2 = 2;
	     
	    printf("Is 1 > 2 : %d\n\n",num1 > num2);

	    if(num1 > num2){
	     
	        printf("%d is greater then %d\n\n", num1, num2);
	     
	    } else if(num1 < num2){
	     
	        printf("%d is less then %d\n\n", num1, num2);
	     
	    } else {
	     
	        printf("%d is equal to %d\n\n", num1, num2);
	     
	    }

	    int custAge = 38;
	     
	    if(custAge > 21 && custAge < 35) printf("They are welcome\n\n");
	    else printf("They are not welcome\n\n");

	    printf("! turns a true into false : %d\n\n", !1);

	    int bobMissedDays = 8, bobTotalSales = 24000, bobNewCust = 32;
	     
	    if(bobMissedDays < 10 && bobTotalSales > 30000 || bobNewCust > 30){
	     
	        printf("Bob gets a raise\n\n");
	     
	    } else {
	     
	        printf("Bob doesn't get a raise\n\n");
	     
	    }

	    char* legalAge = (custAge > 21) ? "true" : "false";
	     
	    printf("Is the customer of legal age? %s\n\n", legalAge);

	    int numOfProducts = 10;
	     
	    printf("I bought %s products\n\n",
	        (numOfProducts > 1) ? "many" : "one");
 
	    printf("A char takes up %d bytes\n\n", sizeof(char));
	     
	    printf("An int takes up %d bytes\n\n", sizeof(int));
	     
	    printf("A long int takes up %d bytes\n\n", sizeof(long int));
	     
	    printf("A float takes up %d bytes\n\n", sizeof(float));
	     
	    printf("A double takes up %d bytes\n\n", sizeof(double));
	     

	    int bigInt = 2147483648;
	     
	    printf("I'm a big Int %d\n\n", bigInt);
	     
	    int numberHowBig = 0;
	     
	    printf("How Many Bits? ");
	     
	    scanf(" %d", &numberHowBig);
	     
	    printf("\n\n");
	     
	    int myIncrementor = 1, myMultiplier = 1, finalValue = 1;
	     
	    while(myIncrementor < numberHowBig){
	     
	        myMultiplier *= 2;
	         
	        finalValue = finalValue + myMultiplier;

	        printf("finalValue: %d myMultiplier: %d myIncrementor: %d\n\n",
	            finalValue, myMultiplier, myIncrementor);
 
	        myIncrementor++;
	     
	    }

	    if ((numberHowBig == 0) || (numberHowBig == 1)){
	     
	        printf("Top Value: %d\n\n", numberHowBig);
	         
	    } else {
	     
	        printf("Top Value: %d\n\n", finalValue);
	         
	    }
	     
	    int secretNumber = 10, numberGuessed = 0;
	     
	    while(1){
	     
	        printf("Guess My Secret Number: ");
	        scanf(" %d", &numberGuessed);
	         
	        if(numberGuessed == 10){
	         
	            printf("You Got It");
	             
	            // break is used to throw you the the first
	            // line of code after the loop
	             
	            break;
	         
	        }
	     
	    }
	     
	    printf("\n\n");
	
	    char sizeOfShirt;
	     
	    do {
	     
	        printf("What Size of Shirt (S,M,L): ");
	     
	        scanf(" %c", &sizeOfShirt);
	     
	    } while(sizeOfShirt != 'S' && sizeOfShirt != 'M' && sizeOfShirt != 'L');
	        
	    for(int counter = 0; counter <= 20; counter++){
	     
	        printf("%d ", counter);
	     
	    }
	     	     
	    printf("\n\n");
	     
	    for(int counter = 0; counter <= 40; counter++){
    
	        if((counter % 2) == 0) continue;
	     
	        printf("%d ", counter);
	     
	    }
	     
	 
	}
