#include <stdio.h>
#include <stdlib.h>

int main(){

  FILE * pFile;

  pFile = fopen("randomwords.txt", "r+");

  char buffer[1000];

  if(!pFile){

      printf("Error : Couldn't Write to File\n");

      return 1;
  }

  fputs("Messing With Strings", pFile);

  // SEEK_SET - Move starting from the beginning of the file

  // SEEK_CUR - Move based off of the current position in the file

  // SEEK_END - Move based off of starting at the end of the file

  
  // Move 12 spaces from the beginning of the file
  fseek(pFile, 12, SEEK_SET);

  // Overwrite characters starting at char number 12
  fputs(" Files  ", pFile);

  
  printf("Success Writing to File\n");

  
  // Move back to the beginning of the file
  fseek(pFile, 0, SEEK_SET);

  // 2 : Move to the end of the file
  fseek(pFile, 0, SEEK_END);


  long numberOfBytes = ftell(pFile);

  printf("Number of Bytes in File : %d\n", numberOfBytes);


  //move backwards 20 bytes in the file 
  fseek(pFile, -20, SEEK_CUR);

  while(fgets(buffer, 1000, pFile) != NULL){

      printf("%s", buffer);
  }

  printf("\n");

  if(fclose(pFile) != 0)

      printf("Error : File Not Closed\n");

  return 0;

}
