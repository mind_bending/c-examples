#include <stdio.h>
#include <stdlib.h>


	int main(){
	 
	    int randomNumber;

	    FILE *pFile;

	    pFile = fopen("randomnumbers.txt", "w");
  
	    if(!pFile){
	     
	        printf("Error : Couldn't Write to File\n");
	         
	        return 1;
	     
	    }
	     
	    // Print 10 random numbers to the file
	     
	    for(int i = 0; i < 10; i++){
	     
	        randomNumber = rand() % 100;
	        fprintf(pFile, "%d\n", randomNumber);
	     
	    }
	     
	    printf("Success Writing to File\n");

	 
	    if(fclose(pFile) != 0)
	        printf("Error : File Not Closed\n");
	 
	    return 0;
	}

