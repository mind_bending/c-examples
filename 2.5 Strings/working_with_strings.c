#include <stdio.h>
#include <string.h>

const char tracks[][80] = {
{"I left my heart in Harvard Med School"},
{"Newark Newark a wonderful town"},
{"Dancing with a Dork"},
{"From here to maternity"},
{"The girl from Iwo Jima"},
};

void find_track(char search_for[])
{
	
	int i;

	for (i = 0; i < 5; i++){
		char* ret;
		ret=strstr(tracks[i], search_for);
		if(ret!=0)
		printf("Track %i: %s\n", i, tracks[i]);
	}
}

int main()
{
	char search[80];
	printf("Search for: ");
	scanf("%s",search, 80, stdin);
	find_track(search);
	return 0;
};
