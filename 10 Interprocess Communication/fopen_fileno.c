#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main()
{
	FILE *my_file = fopen("my_file", "r");
	int descriptor = fileno(my_file);
	printf("I have opened my_file with decriptor number: %i \n", descriptor);

	return 0;

}
