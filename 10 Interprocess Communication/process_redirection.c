#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
//	char *phrase = argv[1];
//	char *vars[] = {"RSS_FEED=http://www.cnn.com/rss/celebs.xml", NULL};
	FILE *f = fopen("somefile", "w");
	if (!f) {
		error("Can't open stories.txt");
	}
printf("The PID is: %i \n",getpid());

	pid_t pid = fork();
	if (pid == -1) {
		error("Can't fork process");
	}
	if (!pid) {
		printf("After fork the PID is %i \n",getpid());
		if (dup2(fileno(f),1)==-1 ) {
			error("Can't redirect Standard Output");
		}
		if (execl("/home/artur/exec_test.sh","/home/artur/exec_test.sh", "Artur",
					"Wieczorek", NULL) == -1) {
			error("Can't run script");
		}

		int pid_status;
		if (waitpid(pid, &pid_status, 0)== -1) {
			error("Error waiting for child process");
	}
		else{
			printf ("The child  process is finished");
		}

	return 0;
}
