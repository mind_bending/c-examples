	#include <stdio.h>
	#include <stdbool.h>
	#include <string.h>
	#include <ctype.h>
 
	void noMoreNewline(char* theString){
	 
	    char * isANewline;
	 
	    // strchr() returns a pointer to the first location
	    // of the character provided
	    // strrchr() returns the last occurance
	     
	    isANewline = strrchr(theString, '\n');
	     
	    if(isANewline){
	     
	        *isANewline = '\0';
	     
	    }
	 
	}
	 
	void makeLowercase(char* theString){
	 
	    int i = 0;
	 
	    while(theString[i]){

	        theString[i] = tolower(theString[i]);
	         
	        i++;
	     
	    }
	 
	}
	 
	void getCharInfo(){
	 
	    char theChar;
	 
	    while ((theChar = getchar()) != '\n'){
	     
	        printf("Letter or Number %d\n\n", isalnum(theChar));
	        printf("Alphabetic Char %d\n\n", isalpha(theChar));
	        printf("Standard Blank %d\n\n", isblank(theChar));
	        printf("Ctrl Char %d\n\n", iscntrl(theChar));
	        printf("Number Char %d\n\n", isdigit(theChar));
	        printf("Anything But Space %d\n\n", isgraph(theChar));
	        printf("Lowercase %d\n\n", islower(theChar));
	        printf("Uppercase %d\n\n", isupper(theChar));
	        printf("Anything not a Letter, Number or Space %d\n\n", ispunct(theChar));
	        printf("Any Space %d\n\n", isspace(theChar));
	     
	    }
	 
	}
	 
	void main(){

	 /* Bool usage example:
	 
	    _Bool isANumber;

	    int number;
	    int sumOfNumbers;
	     
	    printf("Enter a number: ");
  
	    isANumber = (scanf("%d", &number) == 1);
	     
	    while(isANumber){
	     
	        sumOfNumbers = sumOfNumbers + number;
	         
	        printf("Enter another number: ");
	     
	        isANumber = (scanf("%d", &number) == 1);
	     
	    }
	     
	    printf("The Sum is %d\n\n", sumOfNumbers);
	     
	   */
	   
	   
	   /*  putchar(), getchar() usage examples
	   
	    char theChar;
	      
	    while ((theChar = getchar()) != '~'){
	     
	        putchar(theChar);
	     
	    }
		
	    */ 
	     
	     
	     
	    /* gets(), puts(), fgets(), fputs() usage examples
	     
	    char name[50];
	     
	    printf("What is your name? ");
	     
	    gets(name);
	     
	    // puts() receives a string and prints it out followed by
	    // a new line
	     
	    puts("Hi");
	    puts(name);
	     
	    printf("What is your name? ");
	     
	    fgets(name, 50, stdin);
	     
	    // fputs() prints strings without a \n
	     
	    fputs("Hi ", stdout);
	    fputs(name, stdout);
	     
	    */
	     
	    /*
	    // Another way to print strings
	     
	    // Create a string array (pointer to first element in array)
	     
	    char * randomString = "Just some random stuff";
	     
	    // while continues until it reaches a null character, which
	    // with *randomString is equal to 0
	     
	    while(*randomString){
	     
	        // Prints a character in the array and then increments
	        // to the next
	     
	        putchar(*randomString++);
	     
	    }
	     
	    puts("\n");
	     
	    */
	     
	    // The above is the same as
	    /*
	     
	    char * randomString = "Just some random stuff";
	     
	    int i = 0;
	    while(randomString[i] != '\0'){
	        putchar(randomString[i++]);
	    }
	     
	    */
	     
	     
	    char doYouWantToQuit[10];
	     
	    printf("Enter quit to quit: ");
	     
	    fgets(doYouWantToQuit, 10, stdin);
	     
	    noMoreNewline(doYouWantToQuit);
	     
	    makeLowercase(doYouWantToQuit);
	     
	    while(strcmp(doYouWantToQuit, "quit")){
	     
	        printf("Enter quit to quit: ");
	     
	        fgets(doYouWantToQuit, 10, stdin);
	         
	        noMoreNewline(doYouWantToQuit);
	         
	        makeLowercase(doYouWantToQuit);
	     
	    }
	     
	    printf("Thank you for typing %s\n\n",doYouWantToQuit);
	  
	    getCharInfo();
	 
	}