#include <stdio.h>
void my_int_func(int x)
{
    printf( "%d\n", x );
}


int main()
{
    void (*foo)(int);
    foo = &my_int_func;

	//One way to call function:
    foo( 2 );
	//Another way:
    (*foo)( 2 );

    return 0;
}